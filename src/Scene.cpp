#include <cmath>

#include <QMessageBox>
#include <QGLFunctions>
#include <fstream>

#include "CGFunctions.h"
#include "EditWidgets.h"
#include "Light.h"
#include "Model.h"
#include "Scene.h"

inline void OpenGLError() {
	GLenum errCode;
	if ((errCode = glGetError()) != GL_NO_ERROR) {
		qDebug() << "OpenGL Error: \n" << errCode << endl;
	}
}

Scene::Scene(QWidget *parent) : QGLWidget(parent) {
	m_program = nullptr;

	mousepressed = false;
	isDragging = false;

	frame = 0;

	Model::nameCount = 10;
	m_selectedModel = -1;

	showFloor = true;

	// start the update Timer (30fps)
	connect(&updateTimer, SIGNAL(timeout()), this, SLOT(updateGL()));
	updateTimer.start(33);

	OpenGLError();
}

void Scene::resetScene() {
	// delete lights and models
	m_models.clear();
	m_lights.clear();
	m_selectedModel = -1;
	// reset camera
	xRot = 0;
	yRot = 0;
	zRot = 0;
	zoom = -10.0f;
	transx = 0;
	transy = -2.0;
	// load plane
	loadModelFromOBJFile(QString("plane/plane.obj"));
	m_models.back()->rotate(QVector3D(1.0, 0.0, 0.0), -90);
	m_models.back()->scale(10, 0, 10);
}

Scene::~Scene() {
	if (m_program) {
		m_program = 0;
	}
}

void Scene::saveScene(QString filepath) {
	// create File
	std::ofstream file(filepath.toStdString().c_str());

	// 1. write header
	file << m_models.size() - 1 - m_lights.size() << " " << m_lights.size()
	     << "\n";
	file << xRot << " " << yRot << " " << zRot << " "
	     << " " << zoom << " " << transx << " " << transy;
	file << "\n";

	// 2. save Models
	for (size_t m = 1; m < m_models.size(); ++m) {
		std::shared_ptr<Light> l = std::dynamic_pointer_cast<Light>(m_models[m]);
		if (l)
			continue;
		file << m_models[m]->getPath().toStdString() << "\n";
		QMatrix4x4 trafo = m_models[m]->getTransformations();
		for (size_t i = 0; i < 4; ++i)
			for (size_t j = 0; j < 4; ++j)
				file << trafo(i, j) << " ";
		file << "\n";
	}
	file << "\n";

	// 3. save lights
	for (size_t l = 0; l < m_lights.size(); ++l) {
		Light *light = m_lights[l].get();
		QVector3D a = light->getAmbient();
		QVector3D d = light->getDiffuse();
		QVector3D s = light->getSpecular();
		file << a.x() << " " << a.y() << " " << a.z() << "\n";
		file << d.x() << " " << d.y() << " " << d.z() << "\n";
		file << s.x() << " " << s.y() << " " << s.z() << "\n";
		QMatrix4x4 trafo = m_lights[l]->getTransformations();
		for (size_t i = 0; i < 4; ++i)
			for (size_t j = 0; j < 4; ++j)
				file << trafo(i, j) << " ";
		file << "\n";
	}
}

void Scene::loadScene(QString filepath) {
	std::ifstream file(filepath.toStdString().c_str());
	size_t nModels, nLights;
	file >> nModels >> nLights;
	file >> xRot >> yRot >> zRot >> zoom >> transx >> transy;
	float m11, m12, m13, m14, m21, m22, m23, m24, m31, m32, m33, m34, m41, m42,
			m43, m44;

	// read models
	for (size_t m = 0; m < nModels; ++m) {
		std::string path;
		file >> path;
		file >> m11 >> m12 >> m13 >> m14 >> m21 >> m22 >> m23 >> m24 >> m31 >>
		     m32 >> m33 >> m34 >> m41 >> m42 >> m43 >> m44;
		loadModelFromOBJFile(QString(path.c_str()));
		QMatrix4x4 trafo = QMatrix4x4(m11, m12, m13, m14, m21, m22, m23, m24, m31,
		                              m32, m33, m34, m41, m42, m43, m44);
		m_models.back()->setTransformations(trafo);
	}

	// read lights
	for (size_t l = 0; l < nLights; ++l) {
		double ar, ag, ab, dr, dg, db, sr, sg, sb;
		file >> ar >> ag >> ab >> dr >> dg >> db >> sr >> sg >> sb;
		file >> m11 >> m12 >> m13 >> m14 >> m21 >> m22 >> m23 >> m24 >> m31 >>
		     m32 >> m33 >> m34 >> m41 >> m42 >> m43 >> m44;
		QMatrix4x4 trafo = QMatrix4x4(m11, m12, m13, m14, m21, m22, m23, m24, m31,
		                              m32, m33, m34, m41, m42, m43, m44);
		auto light = std::shared_ptr<Light>(new Light(Model::nameCount++));
		light->setAmbient(ar, ag, ab);;
		light->setDiffuse(dr, dg, db);
		light->setSpecular(sr, sg, sb);
		light->setTransformations(trafo);
		m_lights.push_back(light);
		m_models.push_back(m_lights.back());
	}
}

void Scene::deleteModel() {
	if (m_selectedModel == -1)
		return;

	updateTimer.stop();

	// check, if model is also a light
	std::shared_ptr<Light> l = std::dynamic_pointer_cast<Light>(m_models[m_selectedModel]);
	if (l) {
		// delete the corresponding Light
		int name = m_models[m_selectedModel]->getName();
		int lightIndex = -1;
		for (size_t i = 0; i < m_lights.size(); ++i) {
			if (m_lights[i]->getName() == name)
				lightIndex = i;
		}
		if (lightIndex >= 0) {
			std::vector<std::shared_ptr<Light>>::iterator it = m_lights.begin();
			m_lights.erase(it + lightIndex);
		} else
			qDebug() << "Strange error: Did not find light to erase.\n";
	}

	// delete the model
	std::vector<std::shared_ptr<Model>>::iterator it = m_models.begin();
	m_models.erase(it + m_selectedModel);

	// unselect Model
	m_selectedModel = -1;

	updateTimer.start(33);
}

bool Scene::loadModelFromOBJFile(QString path) {
	m_models.push_back(std::shared_ptr<Model>(Model::importOBJFile(path)));
	if (m_models.back()->getNpositions() == 0) {
		QMessageBox::warning(this, QString("Error"),
		                     QString("The file could not be opened."));
		return false;
	}
	m_models.back()->rotate(QVector3D(1.0, 0.0, 0.0), -90);
	return true;
}

void Scene::scaleModel(double sx, double sy, double sz) {
	if (m_selectedModel == -1)
		return;
	m_models[m_selectedModel]->scale(sx, sy, sz);
}

void Scene::translateModel(double dx, double dy, double dz) {
	if (m_selectedModel == -1)
		return;
	m_models[m_selectedModel]->translate(dx, dy, dz);
}

void Scene::rotateModel(double ax, double ay, double az, double angle) {
	if (m_selectedModel == -1)
		return;
	m_models[m_selectedModel]->rotate(QVector3D(ax, ay, az), angle);
}

void Scene::addLight() {
	auto l = std::shared_ptr<Light>(new Light(Model::nameCount++));
	// TODO: set default parameters
	LightDialog ld(l.get());
	if (ld.exec()) {
		m_lights.push_back(l);
		m_models.push_back(m_lights.back());
	}
}

std::shared_ptr<QOpenGLShaderProgram>
Scene::loadShaders(QString vertexShaderSource, QString fragmentShaderSource) {
	auto pr = std::make_shared<QOpenGLShaderProgram>();
	pr->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderSource);
	qDebug() << "Compile VertexShader: ";
	qDebug() << pr->log();

	pr->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderSource);
	qDebug() << "Compile FragmentShader: ";
	qDebug() << pr->log();

	pr->link();
	qDebug() << "Linking Shader Program: ";
	qDebug() << pr->log();

	return pr;
}

void Scene::reloadShader() {
	m_program = loadShaders(
			QString("shader/vertex.glsl"),
			QString("shader/fragment.glsl")
	);
	m_program_highlighted = loadShaders(
			QString("shader/vertex.glsl"),
			QString("shader/fragment_highlighted.glsl")
	);
	m_program_skybox = loadShaders(
			QString("shader/skybox_vertex.glsl"),
			QString("shader/skybox_fragment.glsl")
	);
	m_program_sphere = loadShaders(
			QString("shader/sphere_vertex.glsl"),
			QString("shader/sphere_fragment.glsl")
	);
}

void Scene::setFloor() { showFloor = !showFloor; }

void Scene::initializeGL() {

	vao.create();
	vao.bind();

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_LIGHTING);
	reloadShader();

	resetScene();

	// init the framebuffer object and the cubemap texture object
	int fbo_size = 256;
	for (int i = 0; i < 6; i++) {
		fbo[i] = std::make_shared<QOpenGLFramebufferObject>(fbo_size, fbo_size, GL_TEXTURE_2D);
		fbo[i]->release();
	}
	cubemap_px_format = QOpenGLTexture::PixelFormat::BGRA;
	cubemap_px_type = QOpenGLTexture::PixelType::UInt8;
	cubemap = std::make_shared<QOpenGLTexture>(QOpenGLTexture::TargetCubeMap);
	cubemap->create();
	cubemap->setFormat(QOpenGLTexture::RGBA8_UNorm);
	cubemap->setSize(fbo_size, fbo_size);
	cubemap->setAutoMipMapGenerationEnabled(true);
	cubemap->allocateStorage(cubemap_px_format, cubemap_px_type);
	cubemap->release();

	// init the skybox
	skybox = std::make_shared<Skybox>();

	// create the spheres
	/*
	std::shared_ptr<Sphere> s1 = std::make_shared<Sphere>();
	s1->animation_enable(QVector3D(-4.0f, 0.0f, 0.0f), QVector3D(4.0f, 0.0f, 0.0f), 240);
	m_spheres.emplace_back(s1);
	 */

	std::shared_ptr<Sphere> s2 = std::make_shared<Sphere>(1.5, QVector3D(0.0f, 3.5f, 0.0f));
//	s2->animation_enable(QVector3D(-4.0f, 3.5f, 0.0f), QVector3D(4.0f, 3.5f, 0.0f), 240);
	m_spheres.emplace_back(s2);

	/*
	std::shared_ptr<Sphere> s3 = std::make_shared<Sphere>(2, QVector3D(0.0f, 8.0f, 0.0f));
	s3->animation_enable(QVector3D(-4.0f, 8.0f, 0.0f), QVector3D(4.0f, 8.0f, 0.0f), 240);
	m_spheres.emplace_back(s3);
	 */

	OpenGLError();
}

void Scene::resizeGL(int width, int height) {
	glViewport(0, 0, width, height);
	qreal aspect = qreal(width) / qreal(height ? height : 1);
	const qreal zNear = 0.1f, zFar = 400.0f, fov = 60.0f;
	m_projection.setToIdentity();
	m_projection.perspective(fov, aspect, zNear, zFar);
}

void Scene::mouseMoveEvent(QMouseEvent *event) {
	if (lastPos.x() == -1 || lastPos.y() == -1)
		return;

	int dx = event->x() - lastPos.x();
	int dy = event->y() - lastPos.y();

	if (event->buttons() & Qt::LeftButton && m_selectedModel == -1) {
		setXRotation(xRot + 4 * dy);
		setYRotation(yRot + 4 * dx);
	} else if (event->buttons() & Qt::RightButton) {
		setXRotation(xRot + 4 * dy);
		setZRotation(zRot - 4 * dx);
	} else if (event->buttons() & Qt::MidButton) {
		transx += (double) dx * 0.01;
		transy -= (double) dy * 0.01;
	} else if (event->buttons() & Qt::LeftButton &&
	           m_selectedModel != -1) // move the selected model
	{
		QMatrix4x4 imvpMatrix = (m_projection * m_view).inverted();
		QVector4D center = m_models[m_selectedModel]->getBoundingBox().center;
		// determine the last dragging position
		if (!isDragging) // init with bounding box
		{
			// project the center to view (so that we look in z direction -> z
			// is now the depth)
			QVector4D centerView = m_view * center;
			double z = centerView.z();
			// calculate the depth buffer value of z
			draggingDepth = 2.0 * (z - 0.1) / (1000.0 - 0.1) - 1.0;
			lastDraggingPos = unprojectScreenCoordinates(
					event->x(), event->y(), draggingDepth, width(), height(), imvpMatrix);
			isDragging = true;
		}
		QVector4D draggingPos = unprojectScreenCoordinates(
				event->x(), event->y(), draggingDepth, width(), height(), imvpMatrix);
		QVector4D translation = draggingPos - lastDraggingPos;
		float factor = fabs(zoom * 10);
		m_models[m_selectedModel]->translate(translation.x() * factor,
		                                     translation.y() * factor,
		                                     translation.z() * factor);
		lastDraggingPos = draggingPos;
	}

	lastPos = event->pos();
}

void Scene::wheelEvent(QWheelEvent *event) { zoom -= event->delta() * 0.0025; }

void Scene::mousePressEvent(QMouseEvent *event) {
	mousepressed = true;
	lastPos = event->pos();
}

void Scene::mouseDoubleClickEvent(QMouseEvent *event) {
	// intersect ray with the bounding boxes of all models
	// the functions for this technique are defined in CGFunctions.h

	// calculate intersections of ray in world space
	QMatrix4x4 imvpMatrix = (m_projection * m_view).inverted();
	QVector4D eyeRay_n = unprojectScreenCoordinates(
			event->x(), event->y(), -1.0, width(), height(), imvpMatrix);
	QVector4D eyeRay_z = unprojectScreenCoordinates(
			event->x(), event->y(), 1.0, width(), height(), imvpMatrix);
	float tnear, tfar;
	float smallest_t = 1e33;
	int nearestModel = -1;
	for (size_t i = 0; i < m_models.size(); ++i) {
		BoundingBox bb = m_models[i]->getBoundingBox();
		if (intersectBox(eyeRay_n, eyeRay_z - eyeRay_n, bb.bbmin, bb.bbmax, &tnear,
		                 &tfar)) {
			if (tnear < smallest_t) {
				smallest_t = fabs(tnear);
				nearestModel = i;
			}
		}
	}
	if (nearestModel >= 0 && (nearestModel != m_selectedModel)) {
		m_selectedModel = nearestModel;
		std::cout << "Model: " << m_models[m_selectedModel]->getName()
		          << " was selected\n";
	} else {
		m_selectedModel = -1;
		std::cout << "Model deselected\n";
	}
}

void Scene::mouseReleaseEvent(QMouseEvent *event) {
	mousepressed = false;
	isDragging = false;
}

void Scene::setXRotation(int angle) {
	normalizeAngle(&angle);
	if (angle != xRot) {
		xRot = angle;
	}
}

void Scene::setYRotation(int angle) {
	normalizeAngle(&angle);
	if (angle != yRot) {
		yRot = angle;
	}
}

void Scene::setZRotation(int angle) {
	normalizeAngle(&angle);
	if (angle != zRot) {
		zRot = angle;
	}
}

void Scene::normalizeAngle(int *angle) {
	while (*angle < 0)
		*angle += 360 * 16;
	while (*angle > 360 * 16)
		*angle -= 360 * 16;
}

void Scene::setTransformations() {
	m_view = QMatrix4x4(); // init with idendity matrix
	// do the translation
	m_view.translate(transx, transy, zoom);
	// do the rotation
	m_view.rotate((xRot / 16.0f), 1.0f, 0.0f, 0.0f);
	m_view.rotate((yRot / 16.0f), 0.0f, 1.0f, 0.0f);
	m_view.rotate((zRot / 16.0f), 0.0f, 0.0f, 1.0f);
}

void Scene::collectLightData() {

	// set a flag if we use the camera light
	bool camera_light_on = true;

	// collect and precompute information on the lights so we just need to add them to the shaders, add camera light
	// define variables needed to pass light information to shaders
	int light_count = m_lights.size();
	if (camera_light_on) { light_count++; }

	lights_positions.clear();
	lights_positions.resize(light_count);
	lights_colors_ambient.clear();
	lights_colors_ambient.resize(light_count);
	lights_colors_diffuse.clear();
	lights_colors_diffuse.resize(light_count);
	lights_colors_specular.clear();
	lights_colors_specular.resize(light_count);

	// add fake light on top of the camera
	if (camera_light_on) {
		lights_positions.emplace_back(QVector3D(0.0, 0.0, 0.0));  // origin in camera space == in position of the camera
		lights_colors_ambient.emplace_back(QVector3D(1.0, 1.0, 1.0));
		lights_colors_diffuse.emplace_back(QVector3D(1.0, 1.0, 1.0));
		lights_colors_specular.emplace_back(QVector3D(1.0, 1.0, 1.0));
	}

	// add information of real lights
	for (auto &m_light : m_lights) {
		// get the center of the light and translate it into camera space
		lights_positions.emplace_back(m_view * m_light->getBoundingBox().center);
		lights_colors_ambient.emplace_back(m_light->getAmbient());
		lights_colors_diffuse.emplace_back(m_light->getDiffuse());
		lights_colors_specular.emplace_back(m_light->getSpecular());
	}
}

void Scene::bindLightData(std::shared_ptr<QOpenGLShaderProgram> shader) {
	int light_count = lights_positions.size();
	shader->setUniformValue("i_light_amnt", light_count);
	shader->setUniformValueArray("a_lights_positions", lights_positions.data(), light_count);
	shader->setUniformValueArray("a_lights_colors_ambient", lights_colors_ambient.data(), light_count);
	shader->setUniformValueArray("a_lights_colors_diffuse", lights_colors_diffuse.data(), light_count);
	shader->setUniformValueArray("a_lights_colors_specular", lights_colors_specular.data(), light_count);
}

void Scene::bindGenericVariables(std::shared_ptr<QOpenGLShaderProgram> shader) {
	shader->setUniformValue("b_cel_shading_on", false);
	shader->setUniformValue("i_cel_shading_steps", 8);
}

void Scene::renderSkybox(QMatrix4x4 m_view_local, QMatrix4x4 m_projection_local) {
	// bind the skybox shader and add the generic variables
	m_program_skybox->bind();
	bindGenericVariables(m_program_skybox);

	// bind uniforms
	m_program_skybox->setUniformValue("m_trans_ws_to_ss", m_projection_local * m_view_local);
	QVector3D camera_position = (m_view_local.inverted() * QVector4D(0.0f, 0.0f, 0.0f, 1.0f)).toVector3D();
	m_program_skybox->setUniformValue("camera_position", camera_position);

	// render and release the shader
	skybox->render(m_program_skybox);
	m_program_skybox->release();
}

void Scene::renderModels(QMatrix4x4 m_view_local, QMatrix4x4 m_projection_local) {
	//render all models
	// the floor is always the first model, so if (showFloor == false), we
	// simply start the rendering
	// with the second model
	size_t i;
	for (showFloor ? i = 0 : i = 1; i < m_models.size(); ++i) {
		std::shared_ptr<QOpenGLShaderProgram> shader;
		if (i == m_selectedModel)
			shader = m_program_highlighted;
		else
			shader = m_program;

		// bind the shader and add the generic variables
		shader->bind();
		bindGenericVariables(shader);

		// set transformation matrices, precalculate the transformation matrix into camera space
		QMatrix4x4 m_trans_ms_to_cs = m_view_local * m_models[i]->getTransformations();
		QMatrix4x4 m_trans_ms_to_ss = m_projection_local * m_view_local * m_models[i]->getTransformations();
		shader->setUniformValue(shader->uniformLocation("m_trans_ms_to_cs"), m_trans_ms_to_cs);
		shader->setUniformValue(shader->uniformLocation("m_trans_ms_to_ss"), m_trans_ms_to_ss);

		bindLightData(shader);

		// render the model
		m_models[i]->render(shader);

		// release shader the program
		shader->release();
	}
}

void Scene::renderSpheres() {
	// bind shader and add generic variables
	m_program_sphere->bind();
	bindGenericVariables(m_program_sphere);

	// calculate camera vectors (in world space)
	QVector3D camera_position = (m_view.inverted() * QVector4D(0.0f, 0.0f, 0.0f, 1.0f)).toVector3D();
	// this is done as described here: http://www.lighthouse3d.com/opengl/billboarding/index.php?billCheat2
	QVector3D camera_right_ws = m_view.row(0).toVector3D();
	QVector3D camera_up_ws = m_view.row(1).toVector3D();

	// bind uniforms
	m_program_sphere->setUniformValue("camera_right_ws", camera_right_ws);
	m_program_sphere->setUniformValue("camera_up_ws", camera_up_ws);
	m_program_sphere->setUniformValue("camera_position", camera_position);
	m_program_sphere->setUniformValue("m_view", m_view);
	m_program_sphere->setUniformValue("m_projection", m_projection);
	m_program_sphere->setUniformValue("m_trans_ws_to_ss", m_projection * m_view);
	bindLightData(m_program_sphere);

	// create projection matrix for environment mapping
	QMatrix4x4 m_projection_env = QMatrix4x4();
	m_projection_env.setToIdentity();
	m_projection_env.perspective(90, 1.0, 0.1, 400.0);

	int size[4];
	glGetIntegerv(GL_VIEWPORT, size);

	for (std::shared_ptr<Sphere> s : m_spheres) {
		// release shader for rendering of the environment map
		m_program_sphere->release();

		// render each direction
		for (int i = 0; i < 6; i++) {
			if (!fbo[i]->bind())
				qDebug() << "error binding framebuffer";

			// prepare to render in the framebuffer
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			glViewport(0, 0, fbo[i]->width(), fbo[i]->height());

			// create view matrix
			QMatrix4x4 m_view_env = QMatrix4x4();
			m_view_env.setToIdentity();
			m_view_env.translate(s->v_center);
			m_view_env.lookAt(s->v_center, s->v_center + environment_map_targets[i], environment_map_targets_up[i]);

			// render skybox and models with custom matrices
			renderSkybox(m_view_env, m_projection_env);
			renderModels(m_view_env, m_projection_env);

			// save image from framebuffer to cube map
			cubemap_images[i] = fbo[i]->toImage();
			cubemap->setData(0, 0, environment_map_cubemap_faces[i], cubemap_px_format, cubemap_px_type,
			                 (const void *) cubemap_images[i].constBits());

			if (!fbo[i]->release())
				qDebug() << "error releasing framebuffer";
		}

		// restore original viewport size
		glViewport(size[0], size[1], size[2], size[3]);

		// bind the Sphere shader again
		m_program_sphere->bind();

		// add environment map
		cubemap->bind(0);
		m_program_sphere->setUniformValue("cubemap", 0);

		s->render(m_program_sphere, frame);
	}

	m_program_sphere->release();
}

void Scene::paintGL() {
	++frame;
	if (frame < 0)
		frame = 0;

	// clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// set the view matrix and collect the light data
	setTransformations();
	collectLightData();

	// render the different kinds of stuff
	renderSkybox(m_view, m_projection);
	renderModels(m_view, m_projection);
	renderSpheres();
}
