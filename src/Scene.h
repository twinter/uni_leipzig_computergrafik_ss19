#ifndef SCENE_H
#define SCENE_H

#include <memory>

#include <QMouseEvent>
#include <QOpenGLBuffer>
#include <QOpenGLFramebufferObject>
#include <QOpenGLVertexArrayObject>
#include <QTime>
#include <QTimer>
#include <QVector4D>
#include <QWheelEvent>
#include <QtGui/QOpenGLShaderProgram>
#include <QtGui/qmatrix4x4.h>
#include <QtOpenGL/QGLWidget>
#include <QGLFunctions>
#include "Skybox.h"
#include "Sphere.h"

class Model;

struct Material;

class Light;

class Scene : public QGLWidget, protected QGLFunctions {
Q_OBJECT
public:
	Scene(QWidget *parent = 0);

	~Scene();

	// these functions are called by the GUI, when the user selects the
	// corresponding menu entry
	void scaleModel(double sx, double sy, double sz);

	void translateModel(double dx, double dy, double dz);

	void rotateModel(double ax, double ay, double az, double angle);

	void deleteModel();

	bool loadModelFromOBJFile(QString path);

	void addLight();

	void saveScene(QString filepath);

	void loadScene(QString filepath);

protected:
	void initializeGL();

	void resizeGL(int width, int height);

	void paintGL();

public slots:

	void setXRotation(int angle);

	void setYRotation(int angle);

	void setZRotation(int angle);

	void reloadShader();

	void resetScene();

	void setFloor();

protected slots:

	void mousePressEvent(QMouseEvent *event);

	void mouseMoveEvent(QMouseEvent *event);

	void mouseReleaseEvent(QMouseEvent *event);

	void mouseDoubleClickEvent(QMouseEvent *event);

	void wheelEvent(QWheelEvent *event);

private:
	std::shared_ptr<QOpenGLShaderProgram>
	loadShaders(QString vertexShaderSource, QString fragmentShaderSource);

	// for camera movement
	int xRot;
	int yRot;
	int zRot;
	float zoom;
	float transx;
	float transy;
	QPoint lastPos;
	QVector4D lastDraggingPos;
	float draggingDepth;
	bool isDragging;
	bool mousepressed;

	void normalizeAngle(int *angle);

	QOpenGLVertexArrayObject vao;
	// show the floor
	bool showFloor;

	// the "main" Shader program
	std::shared_ptr<QOpenGLShaderProgram> m_program;
	std::shared_ptr<QOpenGLShaderProgram> m_program_highlighted;
	std::shared_ptr<QOpenGLShaderProgram> m_program_skybox;
	std::shared_ptr<QOpenGLShaderProgram> m_program_sphere;
	// the transformation matrices
	QMatrix4x4 m_view, m_projection;

	// function to set the matrices according to camera movement
	void setTransformations();

	// the skybox
	std::shared_ptr<Skybox> skybox;

	// the models in the scene
	std::vector<std::shared_ptr<Model>> m_models;

	// the lights in the scene
	std::vector<std::shared_ptr<Light>> m_lights;

	// the spheres in the scene
	std::vector<std::shared_ptr<Sphere>> m_spheres;

	// the selected Model
	int m_selectedModel;

	// the timer to repaint the scene
	QTimer updateTimer;

	// to measure the time since the program started (can be used for animation)
	int frame;

	// render functions for different tasks
	void renderSkybox(QMatrix4x4 m_view_local, QMatrix4x4 m_projection_local);
	void renderModels(QMatrix4x4 m_view_local, QMatrix4x4 m_projection_local);
	void renderSpheres();

	// function to be called whenever something concerning the lights changes and associated variables
	void collectLightData();
	std::vector<QVector3D> lights_positions;
	std::vector<QVector3D> lights_colors_ambient;
	std::vector<QVector3D> lights_colors_diffuse;
	std::vector<QVector3D> lights_colors_specular;

	void bindLightData(std::shared_ptr<QOpenGLShaderProgram> shader);

	void bindGenericVariables(std::shared_ptr<QOpenGLShaderProgram> shader);

	// framebuffer object and cubemap for the spheres
	std::shared_ptr<QOpenGLFramebufferObject> fbo[6];
	std::shared_ptr<QOpenGLTexture> cubemap;
	QOpenGLTexture::PixelFormat cubemap_px_format;
	QOpenGLTexture::PixelType cubemap_px_type;
	QImage cubemap_images[6];

	QVector3D environment_map_targets[6] = {
			QVector3D(-1, 0, 0),  // +x
			QVector3D(1, 0, 0),  // -x
			QVector3D(0, 1, 0),  // +y
			QVector3D(0, -1, 0),  // -y
			QVector3D(0, 0, 1),  // +z
			QVector3D(0, 0, -1),  // -z
	};
	QVector3D environment_map_targets_up[6] = {
			QVector3D(0, 1, 0),  // +x
			QVector3D(0, 1, 0),  // -x
			QVector3D(0, 0, -1),  // +y
			QVector3D(0, 0, -1),  // -y
			QVector3D(0, 1, 0),  // +z
			QVector3D(0, 1, 0),  // -z
	};
	QOpenGLTexture::CubeMapFace environment_map_cubemap_faces[6] = {
			QOpenGLTexture::CubeMapFace::CubeMapPositiveX,
			QOpenGLTexture::CubeMapFace::CubeMapNegativeX,
			QOpenGLTexture::CubeMapFace::CubeMapPositiveY,
			QOpenGLTexture::CubeMapFace::CubeMapNegativeY,
			QOpenGLTexture::CubeMapFace::CubeMapPositiveZ,
			QOpenGLTexture::CubeMapFace::CubeMapNegativeZ
	};
};

#endif // SCENE_H
