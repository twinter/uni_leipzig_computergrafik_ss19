#include <QtGui/QOpenGLShaderProgram>
#include "Sphere.h"

Sphere::Sphere() {
	f_radius = 1;
	v_center = QVector3D(0.0, 0.0, 0.0);
	init();
}

Sphere::Sphere(const float r, QVector3D pos) {
	f_radius = r;
	v_center = QVector3D(pos.x(), pos.y(), pos.z());
	init();
}

void Sphere::init() {
	// set generic variables
	v_color = QVector3D(1.0f, 1.0f, 1.0f);
	f_alpha = 1.0f;

	// set remaining variables for Phong shading
	mat_ambient = QVector3D(0.3f, 0.3f, 0.3f);
	mat_diffuse = QVector3D(0.4f, 0.4f, 0.4f);
	mat_specular = QVector3D(0.4f, 0.4f, 0.4f);
	f_shine = 10.0f;

	// set variables for refraction
	refraction_on = true;
	refractive_index = 1.3f;  // refractive index of water as default, this should always be >= 1.0

	// initialize the position buffer
	positionBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
	positionBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
	positionBuffer.create();
	positionBuffer.bind();
	positionBuffer.allocate(&vertices[0], vertices.size() * sizeof(QVector3D));
	positionBuffer.release();
}

void Sphere::render(std::shared_ptr<QOpenGLShaderProgram> program, int frame) {
	// generic uniforms
	program->setUniformValue("f_radius", f_radius);
	if (animation_on) {
		// calculate how far we're on the vector from p2 to p1
		frame = frame % animation_steps;
		float mult = (frame / (float) animation_steps) * 2.0 - 1;
		mult = abs(mult);

		// set the point
		program->setUniformValue("v_center", animation_point2 - (animation_point2 - animation_point1) * mult);
	} else {
		program->setUniformValue("v_center", v_center);
	}

	// uniforms for Phong shading
	program->setUniformValue("v_color", v_color);
	program->setUniformValue("f_alpha", f_alpha);
	program->setUniformValue("mat_ambient", mat_ambient);
	program->setUniformValue("mat_diffuse", mat_diffuse);
	program->setUniformValue("mat_specular", mat_specular);
	program->setUniformValue("f_shine", f_shine);

	// uniforms for refraction
	program->setUniformValue("b_refraction_on", refraction_on);
	program->setUniformValue("f_refractive_index", refractive_index);

	// set vertex positions
	int positionAttributeID = program->attributeLocation("position");
	program->enableAttributeArray(positionAttributeID);
	positionBuffer.bind();
	program->setAttributeBuffer(positionAttributeID, GL_FLOAT, 0, 3);
	positionBuffer.release();

	// draw
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
}

void Sphere::animation_disable() {
	animation_on = false;
}

void Sphere::animation_enable(QVector3D point1, QVector3D point2, int duration) {
	animation_on = true;
	animation_point1 = point1;
	animation_point2 = point2;
	animation_steps = duration;
}
