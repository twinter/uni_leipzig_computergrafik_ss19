#include "Skybox.h"
#include <cmath>
#include <fstream>
#include <iostream>
#include <QMessageBox>
#include "Model.h"

using namespace std;

Skybox::Skybox() {

	// the position buffer
	positionBuffer = QOpenGLBuffer(QOpenGLBuffer::VertexBuffer);
	positionBuffer.setUsagePattern(QOpenGLBuffer::StaticDraw);
	positionBuffer.create();
	positionBuffer.bind();
	positionBuffer.allocate(&positions[0], positions.size() * sizeof(QVector3D));
	positionBuffer.release();

	// An dieser Stelle wird wieder die nullte Texturebene genutzt.
	glActiveTexture(GL_TEXTURE0);

	skyImages.resize(6);

	skyImages[0] = std::unique_ptr<QImage>(new QImage(QString("skybox/right.png")));
	skyImages[1] = std::unique_ptr<QImage>(new QImage(QString("skybox/down.png")));
	skyImages[2] = std::unique_ptr<QImage>(new QImage(QString("skybox/back.png")));
	skyImages[3] = std::unique_ptr<QImage>(new QImage(QString("skybox/left.png")));
	skyImages[4] = std::unique_ptr<QImage>(new QImage(QString("skybox/up.png")));
	skyImages[5] = std::unique_ptr<QImage>(new QImage(QString("skybox/front.png")));

	// create textureCube with skybox images
	QOpenGLTexture::PixelFormat px_format = QOpenGLTexture::PixelFormat::BGRA;
	QOpenGLTexture::PixelType px_type = QOpenGLTexture::PixelType::UInt8;
	int mip_layer = 0;
	texture = std::make_shared<QOpenGLTexture>(QOpenGLTexture::TargetCubeMap);
	texture->create();
	texture->setFormat(QOpenGLTexture::RGBA8_UNorm);
	texture->setSize(2048, 2048);
	texture->setAutoMipMapGenerationEnabled(true);
	texture->allocateStorage(px_format, px_type);
	texture->setData(mip_layer, 0, QOpenGLTexture::CubeMapFace::CubeMapNegativeX, px_format, px_type, (const void*)skyImages[0]->constBits());
	texture->setData(mip_layer, 0, QOpenGLTexture::CubeMapFace::CubeMapNegativeY, px_format, px_type, (const void*)skyImages[1]->constBits());
	texture->setData(mip_layer, 0, QOpenGLTexture::CubeMapFace::CubeMapNegativeZ, px_format, px_type, (const void*)skyImages[2]->constBits());
	texture->setData(mip_layer, 0, QOpenGLTexture::CubeMapFace::CubeMapPositiveX, px_format, px_type, (const void*)skyImages[3]->constBits());
	texture->setData(mip_layer, 0, QOpenGLTexture::CubeMapFace::CubeMapPositiveY, px_format, px_type, (const void*)skyImages[4]->constBits());
	texture->setData(mip_layer, 0, QOpenGLTexture::CubeMapFace::CubeMapPositiveZ, px_format, px_type, (const void*)skyImages[5]->constBits());
	texture->release();
}

void Skybox::render(std::shared_ptr <QOpenGLShaderProgram> program) {
	// bind and release happen outside of this function

	// disable depth test, save previous state and bind the shader
	bool enable_depth_test = glIsEnabled(GL_DEPTH_TEST);
	glDisable(GL_DEPTH_TEST);

	// set vertex positions
	int positionAttributeID = program->attributeLocation("position");
	program->enableAttributeArray(positionAttributeID);
	positionBuffer.bind();
	program->setAttributeBuffer(positionAttributeID, GL_FLOAT, 0, 3);
	positionBuffer.release();

	// set texture
	texture->bind(0);
	program->setUniformValue("textureCube", 0);

	// draw
	glDrawArrays(GL_TRIANGLES, 0, positions.size());

	// restore previous depth test state
	if (enable_depth_test) glEnable(GL_DEPTH_TEST);

}
