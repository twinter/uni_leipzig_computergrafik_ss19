#ifndef CGVIEWER_SPHERE_H
#define CGVIEWER_SPHERE_H


#include <memory>
#include <QtGui/QVector3D>
#include <QtGui/QOpenGLBuffer>

class Sphere {
public:
	Sphere();

	Sphere(float r, QVector3D pos);

	void render(std::shared_ptr<QOpenGLShaderProgram> program, int frame);
	void animation_enable(QVector3D point1, QVector3D point2, int duration);
	void animation_disable();

	float f_radius;
	QVector3D v_center;

	// properties used for Phong shading
	QVector3D v_color;
	float f_alpha;
	QVector3D mat_ambient;
	QVector3D mat_diffuse;
	QVector3D mat_specular;
	float f_shine;

	// properties used for refraction
	bool refraction_on;
	float refractive_index;

private:
	void init();

	QOpenGLBuffer positionBuffer;
	const std::vector<QVector3D> vertices = {
			QVector3D(-1.0f, -1.0f, 0.0f),  // top-left triangle
			QVector3D(1.0f, -1.0f, 0.0f),
			QVector3D(1.0f, 1.0f, 0.0f),

			QVector3D(-1.0f, -1.0f, 0.0f), // bottom-right triangle
			QVector3D(1.0f, 1.0f, 0.0f),
			QVector3D(-1.0f, 1.0f, 0.0f),
	};

	// properties for animating the sphere
	bool animation_on = false;
	QVector3D animation_point1;
	QVector3D animation_point2;
	int animation_steps;
};


#endif //CGVIEWER_SPHERE_H
