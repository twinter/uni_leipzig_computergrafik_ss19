#version 330 core
#define MAX_LIGHTS 128
#define TOTAL_REFLECTION_PASSES 10

in vec3 position_ws;
in vec3 position_cs;
flat in vec3 center_cs;
flat in vec3 col_ambient;

// generic uniforms
uniform float f_radius;
uniform vec3 v_center;
uniform vec3 v_color;
uniform mat4 m_projection;

// light related uniforms
uniform int i_light_amnt;
uniform vec3 a_lights_positions[MAX_LIGHTS];
uniform vec3 a_lights_colors_diffuse[MAX_LIGHTS];
uniform vec3 a_lights_colors_specular[MAX_LIGHTS];
uniform vec3 mat_diffuse;
uniform vec3 mat_specular;
uniform float f_alpha;
uniform float f_shine;

// variables related to refraction
uniform bool b_refraction_on;
uniform float f_refractive_index;
uniform vec3 camera_position;
uniform samplerCube cubemap;

// generic variables
uniform bool b_cel_shading_on;
uniform int i_cel_shading_steps;

out vec4 finalColor;

// this assumes that the given ray will actually hit the sphere
void intersect_sphere_ray(in vec3 p, in vec3 d, in vec3 sphere_center, in float radius, out vec3 intersection_point) {
	// TODO: rename the variables to something more readable
	// calculate the intersection point of the ray from the camera with the sphere
	// implementation described as in here: https://gamedev.stackexchange.com/a/96487
	vec3 m = p - sphere_center;
	float b = dot(m, d);
	float c = dot(m, m) - radius * radius;
	float discr = b * b - c;
	float t = -b - sqrt(discr);
	t = max(t, 0.0);
	intersection_point = p + t * d;  // calculate the point on the surface of the sphere hit by the current ray
}

// this calculates everything in camera space
void calculate_phong_shading_color(in vec3 positions_cs, in vec3 v_normal, out vec4 col_final) {
	// assumes the ambient lighting calculations are already done and present

	// calculate i_light_amount again (in case MAX_LIGHT is different here)
	int i_light_count = MAX_LIGHTS;
	if (i_light_amnt < MAX_LIGHTS) { i_light_count = i_light_amnt; }

	// calculate diffuse and specular lighting
	vec3 col_diffuse = vec3(0.0, 0.0, 0.0);
	vec3 col_specular = vec3(0.0, 0.0, 0.0);
	vec3 v_position_to_camera = normalize(vec3(0.0) - position_cs);// the vector to the camera is always from origin to position
	for (int i=0; i < i_light_count; i++) {
		// normalized vector from light source to position (all in camera space)
		vec3 v_position_to_light = a_lights_positions[i] - position_cs;
		v_position_to_light = normalize(v_position_to_light);

		// calculate diffuse lighting for this light source
		col_diffuse += mat_diffuse * a_lights_colors_diffuse[i] * max(dot(v_position_to_light, v_normal), 0);

		// calculate the reflection vector
		vec3 v_reflection = 2 * max(dot(v_position_to_light, v_normal), 0) * v_normal - v_position_to_light;

		// calculate specular lighting
		col_specular += mat_specular * a_lights_colors_specular[i] * pow(max(dot(v_reflection, v_position_to_camera), 0), f_shine);
	}

	// add the parts to calculate the final color of the pixel and multiply with sphere color to get final color
	col_final = vec4(col_ambient + col_diffuse + col_specular, 1.0);
}

// this calculates everything in world space to simplify getting the texture from the skybox
// it assumes that the given refraction index is >= 1.0
void calculate_refraction_color(out vec4 col_final) {
	// set ray origin and direction
	vec3 v_origin = camera_position;
	vec3 v_direction = normalize(position_ws - camera_position);

	// calculate the surface point and normal again, this time in world space
	vec3 v_surface_point_ws = vec3(0);
	intersect_sphere_ray(v_origin, v_direction, v_center, f_radius, v_surface_point_ws);
	vec3 v_normal_ws = normalize(v_surface_point_ws - v_center);

	// first refraction
	v_direction = refract(v_direction, v_normal_ws, 1.0 / f_refractive_index);

	for (int i = 0; i < TOTAL_REFLECTION_PASSES; i++) {
		// set new ray origin and recalculate surface point and normal
		v_origin = v_surface_point_ws;
		v_direction = normalize(v_direction);
		intersect_sphere_ray(v_origin, v_direction, v_center, f_radius, v_surface_point_ws);
		v_normal_ws = normalize(v_surface_point_ws - v_center);

		// try refraction
		vec3 v_refraction_direction = refract(v_direction, -1 * v_normal_ws, f_refractive_index);

		// exit the loop if the refraction was sucessful
		if (v_refraction_direction != vec3(0)) {
			v_direction = v_refraction_direction;
			break;
		}

		// handle total internal reflection
		v_direction = reflect(v_direction, v_normal_ws);
	}

	// get color info from skybox texture (origin of ray = center of skybox)
	col_final = texture(cubemap, v_direction);

	// mark areas of total reflection
	if (v_direction == vec3(0)) {
		col_final = vec4(1, 0, 0, 1);
	}
}

void main(void) {
	// calculate vector from the center of the sphere to the current position
	vec3 center_to_pos = center_cs - position_cs;

	// check if we're in the circle
	if (length(center_to_pos) > f_radius) { discard; }

	// calculate the intersection point of the ray from the camera with the sphere
	vec3 v_surface_point = vec3(0.0);
	intersect_sphere_ray(vec3(0.0), normalize(position_cs), center_cs, f_radius, v_surface_point);

	// calculate the normalized normal
	vec3 v_normal = v_surface_point - center_cs;
	v_normal = normalize(v_normal);

	vec4 col_final = vec4(1.0);
	if (b_refraction_on) {
		calculate_refraction_color(col_final);
	} else {
		calculate_phong_shading_color(v_surface_point, v_normal, col_final);
	}
	col_final.rgb *= v_color;
	finalColor = vec4(col_final.rgb, col_final.a * f_alpha);

	// set the NDC depth of the current fragment
	// based on the formula from https://stackoverflow.com/a/12904072/5945775
	vec4 v_surface_point_ss_4d = m_projection * vec4(v_surface_point, 1.0);
	float near = gl_DepthRange.near;
	float far = gl_DepthRange.far;
	float ndc_depth = v_surface_point_ss_4d.z / v_surface_point_ss_4d.w;
	gl_FragDepth = (((far - near) * ndc_depth) + near + far) * 0.5;

	// apply cel shading
	if (b_cel_shading_on) {
		if (dot(v_normal, normalize(-1 * position_cs)) < 0.3) {
			finalColor = vec4(0.0, 0.0, 0.0, finalColor.a);
		} else {
			finalColor.r = round(finalColor.r * i_cel_shading_steps) / i_cel_shading_steps;
			finalColor.g = round(finalColor.g * i_cel_shading_steps) / i_cel_shading_steps;
			finalColor.b = round(finalColor.b * i_cel_shading_steps) / i_cel_shading_steps;
		}
	}
}