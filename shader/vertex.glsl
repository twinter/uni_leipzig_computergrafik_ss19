#version 330 core
#define MAX_LIGHTS 128

// vertex attributes
in vec3 position;
in vec3 normal;
in vec2 texCoords;

// uniforms
uniform mat4 m_trans_ms_to_cs;
uniform mat4 m_trans_ms_to_ss;
uniform int i_light_amnt;
uniform vec3 a_lights_colors_ambient[MAX_LIGHTS];
uniform vec3 matAmbient;

// outputs, passed to fragment shader
out vec3 position_cs;
out vec3 normal_cs;
out vec3 col_ambient;
out vec2 texCoords_copy;

void main(void)
{
	int i_light_count = MAX_LIGHTS;
	if (i_light_amnt < MAX_LIGHTS) {
		i_light_count = i_light_amnt;
	}

	// calculate the position in camera space
	vec4 position_cs_4d = m_trans_ms_to_cs * vec4(position, 1.0);
	position_cs = position_cs_4d.xyz;

	// transform vertex position into screen space
	gl_Position = m_trans_ms_to_ss * vec4(position, 1.0);

	// calculate ambient light
	col_ambient = vec3(0.0, 0.0, 0.0);
	for (int i=0; i < i_light_count; i++) {
		col_ambient += matAmbient * a_lights_colors_ambient[i];
	}

	// convert to normal from model space to camera space
	vec4 normal_cs_4d = m_trans_ms_to_cs * vec4(normal, 0.0);
	normal_cs = normal_cs_4d.xyz;

	// copy the texture coordinates as we don't seem to be able to just pass them through
	texCoords_copy = texCoords;
}

