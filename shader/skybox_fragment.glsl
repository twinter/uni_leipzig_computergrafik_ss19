# version 330 core

in vec3 position_copy;

uniform samplerCube texture_cube;

// generic variables
uniform bool b_cel_shading_on;
uniform int i_cel_shading_steps;

out vec4 finalColor;

void main(void) {
	finalColor = texture(texture_cube, position_copy);

	// apply cel shading
	if (b_cel_shading_on) {
		finalColor.r = round(finalColor.r * i_cel_shading_steps) / i_cel_shading_steps;
		finalColor.g = round(finalColor.g * i_cel_shading_steps) / i_cel_shading_steps;
		finalColor.b = round(finalColor.b * i_cel_shading_steps) / i_cel_shading_steps;
	}
}