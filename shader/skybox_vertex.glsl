# version 330 core

in vec3 position;

uniform vec3 camera_position;
uniform mat4 m_trans_ws_to_ss;

out vec3 position_copy;

void main(void) {
	// move to the camera (both are in world space in world space), this is the final world space position
	vec3 position_ws = position + camera_position;

	// translate the coordinates to screen space
	gl_Position = m_trans_ws_to_ss * vec4(position_ws, 1.0);

	position_copy = position;
}