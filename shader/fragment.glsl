#version 330 core
#define MAX_LIGHTS 128

// stuff passed from vertex shader
in vec3 position_cs;
in vec3 normal_cs;
in vec3 col_ambient;
in vec2 texCoords_copy;

// uniforms
uniform int i_light_amnt;
uniform vec3 a_lights_positions[MAX_LIGHTS];
uniform vec3 a_lights_colors_diffuse[MAX_LIGHTS];
uniform vec3 a_lights_colors_specular[MAX_LIGHTS];
uniform vec3 matDiffuse;
uniform vec3 matSpecular;
uniform float shine;
uniform float alpha;
uniform sampler2D textureSampler;

// generic variables
uniform bool b_cel_shading_on;
uniform int i_cel_shading_steps;

// output of fragment shader
out vec4 finalColor;

void main(void)
{
	// debug flag, pixel will be set to hot pink if true
	bool flag = false;

	// calculate i_light_amount again (in case MAX_LIGHT is different here)
	int i_light_count = MAX_LIGHTS;
	if (i_light_amnt < MAX_LIGHTS) {
		i_light_count = i_light_amnt;
	}

	// get the color from the texture
	vec3 col_texture = texture(textureSampler, texCoords_copy).rgb;

	// normalize the normal
	vec3 v_normal = normalize(normal_cs);

	// calculate diffuse and specular lighting
	vec3 col_diffuse = vec3(0.0, 0.0, 0.0);
	vec3 col_specular = vec3(0.0, 0.0, 0.0);
	vec3 v_position_to_camera = normalize(-1 * position_cs);// the vector to the camera is always from origin to position
	for (int i=0; i < i_light_count; i++) {
		// normalized vector from light source to position (all in camera space)
		vec3 v_position_to_light = a_lights_positions[i] - position_cs;
		v_position_to_light = normalize(v_position_to_light);

		// calculate diffuse lighting for this light source
		col_diffuse += matDiffuse * a_lights_colors_diffuse[i] * max(dot(v_position_to_light, v_normal), 0);

		// calculate the reflection vector
		vec3 v_reflection = 2 * max(dot(v_position_to_light, v_normal), 0) * v_normal - v_position_to_light;

		// calculate specular lighting
		col_specular += matSpecular * a_lights_colors_specular[i] * pow(max(dot(v_reflection, v_position_to_camera), 0), shine);
	}

	// add the parts to calculate the final color of the pixel and multiply with the texture color
	vec3 col_final = col_ambient + col_diffuse + col_specular;
	if (texCoords_copy.x >= 0 && texCoords_copy.y >= 0) {col_final *= col_texture;}

	finalColor = vec4(col_final, alpha);

	// apply cel shading
	if (b_cel_shading_on) {
		if (dot(v_normal, v_position_to_camera) < 0.3) {
			finalColor = vec4(0.0, 0.0, 0.0, finalColor.a);
		} else {
			finalColor.r = round(finalColor.r * i_cel_shading_steps) / i_cel_shading_steps;
			finalColor.g = round(finalColor.g * i_cel_shading_steps) / i_cel_shading_steps;
			finalColor.b = round(finalColor.b * i_cel_shading_steps) / i_cel_shading_steps;
		}
	}

	// replace with highlight color if flag ist set
	if (flag) {
		finalColor = vec4(1.0, 0.41, 0.71, 1.0);
	}
}

