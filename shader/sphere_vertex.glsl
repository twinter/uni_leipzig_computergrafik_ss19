#version 330 core
#define MAX_LIGHTS 128

in vec3 position;

// generic uniforms
uniform float f_radius;
uniform vec3 v_center;
uniform vec3 camera_right_ws;
uniform vec3 camera_up_ws;
uniform mat4 m_view;
uniform mat4 m_trans_ws_to_ss;

// uniforms for Phong shading
uniform int i_light_amnt;
uniform vec3 a_lights_colors_ambient[MAX_LIGHTS];
uniform vec3 mat_ambient;

// uniforms for refraction
uniform bool b_refraction_on;

out vec3 position_cs;
out vec3 position_ws;
flat out vec3 center_cs;
flat out vec3 col_ambient;

void main(void) {
	// move the vertex so the billboard is perpendicular to the camera
	vec3 position_shifted_ws = v_center;
	position_shifted_ws += camera_right_ws * position.x * f_radius;
	position_shifted_ws += camera_up_ws * position.y * f_radius;

	// set the vertex position in screen space
	gl_Position = m_trans_ws_to_ss * vec4(position_shifted_ws, 1.0);

	// calculate the position and center in camera space
	vec4 position_cs_4d = m_view * vec4(position_shifted_ws, 1.0);
	position_cs = position_cs_4d.xyz;
	vec4 center_cs_4d = m_view * vec4(v_center, 1.0);
	center_cs = center_cs_4d.xyz;

	// copy corrected world space position
	position_ws = position_shifted_ws;

	if (b_refraction_on) {
		// nothing to do here if refraction is on
		col_ambient = vec3(0.0);
	} else {
		// calculate ambient light if refraction is off
		int i_light_count = MAX_LIGHTS;
		if (i_light_amnt < MAX_LIGHTS) { i_light_count = i_light_amnt; }
		col_ambient = vec3(0.0, 0.0, 0.0);
		for (int i=0; i < i_light_count; i++) {
			col_ambient += mat_ambient * a_lights_colors_ambient[i];
		}
	}
}