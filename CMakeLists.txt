cmake_minimum_required(VERSION 3.10)
project(CGViewer)

set(CMAKE_CXX_STANDARD 14)

# run moc, uic and rcc automatically when needed
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

# find includes in corresponding build directories
# TODO: remove the following line
#set(CMAKE_INCLUDE_CURRENT_DIR ON)

# include everything that may have files relevant to cmake in them
include_directories(. src moc plane shader)

# path to Qt doesn't need to be set explicitly on my system but do it here just to be safe
set(CMAKE_PREFIX_PATH "/lib/qt/mkspecs/features/data/cmake/")

# select OpenGL package and ensure availability
set(OpenGL_GL_PREFERENCE "GLVND")
find_package(OpenGL REQUIRED)

# ensure availability of relevant Qt packages
find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(Qt5OpenGL REQUIRED)

set(SOURCES
        src/main.cpp
        src/Mainwindow.cpp
        src/Scene.cpp
        src/EditWidgets.cpp
        src/Model.cpp
        src/Light.cpp
        src/Skybox.cpp
        src/Sphere.cpp)
set(HEADERS
        src/Mainwindow.h
        src/CGTypes.h
        src/Scene.h
        src/EditWidgets.h
        src/Model.h
        src/Light.h
        src/Skybox.h
        src/CGFunctions.h
        src/Sphere.h)
add_executable(CGViewer ${SOURCES} ${HEADERS} resources.qrc)

target_link_libraries(CGViewer OpenGL)
target_link_libraries(CGViewer Qt5::Core)
target_link_libraries(CGViewer Qt5::Widgets)
target_link_libraries(CGViewer Qt5::Quick)
target_link_libraries(CGViewer Qt5::OpenGL)
