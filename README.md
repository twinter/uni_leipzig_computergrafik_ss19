# CGViewer

The CGViewer program with custom additions as required by the "Computergrafik" course at the Universität Leipzig in the summer semester 2019.

## build & execute

Run `qmake . && make -j4 && ./CGViewer` in the root folder of the project. Feel free to substitute the "4" in `make -j4` by the number of cores you have.
